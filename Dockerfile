FROM node:8-slim

LABEL maintainer=fivecorp

RUN DEBIAN_FRONTEND=noninteractive apt-get update -qq && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y bzip2 libfontconfig1 && \
    apt-get clean purge

# Install PhantomJS last version
RUN cd /tmp && \
    curl -L https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 --output phantomjs.tar.bz2 && \
    tar -xjvf phantomjs.tar.bz2 && \
    mv phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/bin/ && \
    rm -rf /tmp/*
